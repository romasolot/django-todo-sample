# TODO-APP

**Demo:** http://todoapp.hosting.zerp.info:8001/

User1
* Login: user1
* Password: password11

User2
* Login: user2
* Password: password22

Or create a new user by link http://todoapp.hosting.zerp.info:8001/account/registration


How to install
============================

With using virtualenv.

```bash
sudo pip install virtualenv
mkdir /APP_PATH
cd /APP_PATH
git clone https://romasolot@bitbucket.org/romasolot/django-todo-sample.git .
virtualenv .env
source .env/bin/activate
pip install -r requirements.txt
./manage.py makemigrations
./manage.py migrate
./manage.py collectstatic --noinput
```

Config APP
------------------------------
Add hostname to ALLOWED_HOSTS in todoapp/settings.py

Test APP
------------------------------
```bash
cd /APP_PATH
./manage.py test
```

Run APP
------------------------------
```bash
cd /APP_PATH
./manage.py runserver
```