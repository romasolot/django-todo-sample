from django.shortcuts import redirect
from django.urls import reverse


def index_view(request):
    if request.user:
        redirect_url = reverse('Todo:list')
    else:
        redirect_url = reverse('Account:login')

    return redirect(redirect_url)
