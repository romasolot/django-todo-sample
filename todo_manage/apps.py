from django.apps import AppConfig


class TodoManageConfig(AppConfig):
    name = 'todo_manage'
