from django.contrib.auth.models import User
from django.db import models


class TodoQuerySet(models.QuerySet):
    def active(self):
        return self.filter(status='UNDONE')


class Todo(models.Model):
    TODO_STATUSES = (
        ('DONE', 'Done'),
        ('UNDONE', 'Undone'),
    )

    user = models.ForeignKey(User)
    name = models.CharField(max_length=100)
    description = models.TextField()
    status = models.CharField(choices=TODO_STATUSES, default='UNDONE', max_length=6)
    created_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)

    objects = TodoQuerySet.as_manager()

    class Meta:
        ordering = ['-created_at']

    def __str__(self):
        return 'Task #' + str(self.id)

    def is_done(self):
        return self.status == 'DONE'

    def mark_as_done(self, user=None):
        self.status = 'DONE'
        if hasattr(self, 'related_done'):
            self.related_done.user = user
        else:
            TodoDone.objects.create(todo=self, user=user)
        self.save()


class TodoDone(models.Model):
    todo = models.OneToOneField(Todo, related_name='related_done')
    user = models.ForeignKey(User)
    updated_at = models.DateTimeField(auto_now_add=True, blank=True, null=True)
