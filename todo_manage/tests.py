from django.contrib.auth.models import User
from django.test import Client
from django.test import RequestFactory
from django.test import TestCase
from django.urls import reverse

from todo_manage.models import Todo
from todo_manage.views import TodoListView, TodoCreateView


class TestTodoModel(TestCase):
    def setUp(self):
        user = User.objects.create_user('testuser', 'testuser@test.loc', 'testpassword')
        Todo.objects.create(user=user, name='Test Name Todo 1', description='Test Todo Description', status='DONE')
        Todo.objects.create(user=user, name='Test Name Todo 2', description='Test Todo Description', status='UNDONE')

        self.user = user

    def test_get_all_objects(self):
        all_objects = Todo.objects.all()
        self.assertEqual(all_objects.count(), 2)

    def test_get_undone_objects(self):
        undone_objects = Todo.objects.active().all()
        self.assertEqual(undone_objects.count(), 1)

        obj = undone_objects.first()
        self.assertEqual(obj.status, 'UNDONE')
        self.assertEqual(obj.name, 'Test Name Todo 2')
        self.assertEqual(obj.is_done(), False)

    def test_mark_as_done_object(self):
        obj = Todo.objects.active().first()

        obj.mark_as_done(self.user)
        self.assertEqual(obj.status, 'DONE')
        self.assertEqual(obj.is_done(), True)

        self.assertEqual(obj.related_done.user, self.user)
        self.assertIsNotNone(obj.related_done.updated_at)


class AccessToPageTest(TestCase):
    client = Client()

    def test_access_to_list(self):
        response = self.client.get(reverse('Todo:list'))
        redirect_url = reverse('Account:login') + '?next=' + reverse('Todo:list')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, redirect_url)

    def test_access_to_add(self):
        response = self.client.get(reverse('Todo:add'))
        redirect_url = reverse('Account:login') + '?next=' + reverse('Todo:add')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, redirect_url)

    def test_access_to_edit(self):
        target_url = reverse('Todo:edit', kwargs={'pk': '999999999'})
        response = self.client.get(target_url)
        redirect_url = reverse('Account:login') + '?next=' + target_url
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.url, redirect_url)


class AccessWithAuth(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user('testuser', 'testuser@test.loc', 'testpassword')

    def test_manage_page(self):
        request = self.factory.get(reverse('Todo:list'))
        request.session = {}
        request.user = self.user
        response = TodoListView.as_view()(request)


class Test_TodoListView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user('testuser', 'testuser@test.loc', 'testpassword')
        self.request = self.factory.get(reverse('Todo:list'))
        self.request.session = {}
        self.request.user = self.user

    def test_get(self):
        response = TodoListView.as_view()(self.request)
        self.assertEqual(response.status_code, 200)
        self.assertNotContains(response, 'Hide Completed tasks')
        self.assertNotContains(response, 'Show All tasks')
        self.assertContains(response, 'No tasks yet.')


class Test_TodoCreateView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user('testuser', 'testuser@test.loc', 'testpassword')
        self.request = self.factory.get(reverse('Todo:add'))
        self.request.session = {}
        self.request.user = self.user

    def test_get(self):
        response = TodoCreateView.as_view()(self.request)
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Add Todo')
