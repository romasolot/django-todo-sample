from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import Http404
from django.shortcuts import redirect
from django.urls import reverse
from django.views import View
from django.views.generic import CreateView, DeleteView, ListView, UpdateView

from todo_manage.forms import TodoForm
from todo_manage.models import Todo


class TodoListView(LoginRequiredMixin, ListView):
    template_name = 'todo_manage/todo_list.html'
    paginate_by = 5

    def get_queryset(self):
        queryset = Todo.objects.all()
        target_filter = self.request.session.get('filter')
        if target_filter == 'active':
            queryset = queryset.active()
        return queryset

    def get(self, request, *args, **kwargs):
        target_filter = self.request.GET.get('filter')
        if target_filter:
            request.session['filter'] = target_filter
        return super(TodoListView, self).get(self, request, *args, **kwargs)


class TodoFormView(LoginRequiredMixin):
    form_class = TodoForm
    template_name = 'todo_manage/todo_form.html'

    def __init__(self):
        self.request = None

    def get_success_url(self):
        return reverse('Todo:list')

    def get_queryset(self):
        return Todo.objects.all().filter(user=self.request.user)

    def form_valid(self, form):
        form.save(commit=False)
        form.instance.user = self.request.user
        save_result = super(TodoFormView, self).form_valid(form)
        if form.instance.is_done():
            form.instance.mark_as_done(self.request.user)
        return save_result


class TodoCreateView(TodoFormView, CreateView):
    pass


class TodoUpdateView(TodoFormView, UpdateView):
    def get(self, request, *args, **kwargs):
        try:
            self.object = self.get_object()
        except Http404:
            messages.warning(request, 'Todo #' + kwargs.get('pk') + ' was not found for editing.')
            return redirect(reverse('Todo:list'))
        context = self.get_context_data(object=self.object)
        return self.render_to_response(context)


class TodoDeleteView(LoginRequiredMixin, DeleteView):
    def get_success_url(self):
        return reverse('Todo:list')

    def get_queryset(self):
        return Todo.objects.all().filter(user=self.request.user)


class TodoDoneView(LoginRequiredMixin, View):
    queryset = Todo.objects.all()

    def get(self, request, pk=None):
        try:
            self.queryset.get(pk=pk).mark_as_done(user=self.request.user)
            messages.success(request, 'Todo #' + pk + ' has been marked as Done.')
            return redirect(reverse('Todo:list'))
        except Todo.DoesNotExist:
            messages.warning(request, 'Todo #' + pk + ' was not found.')
            return redirect(reverse('Todo:list'))
