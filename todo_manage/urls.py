from django.conf.urls import url
from django.conf.urls.static import static

from todo_manage.views import TodoListView, TodoDeleteView, TodoDoneView, TodoCreateView, TodoUpdateView
from todoapp import settings

urlpatterns = [
                  url(r'^$', TodoListView.as_view(), name='list'),
                  url(r'^add$', TodoCreateView.as_view(), name='add'),
                  url(r'^edit/(?P<pk>[0-9]+)$', TodoUpdateView.as_view(), name='edit'),
                  url(r'^delete/(?P<pk>[0-9]+)$', TodoDeleteView.as_view(), name='delete'),
                  url(r'^done/(?P<pk>[0-9]+)$', TodoDoneView.as_view(), name='done')
              ] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
