from django.apps import AppConfig


class TodoAuthConfig(AppConfig):
    name = 'todo_auth'
