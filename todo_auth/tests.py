from django.test import Client
from django.test import RequestFactory
from django.test import TestCase
from django.urls import reverse


class AuthTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()

    def test_page(self):
        c = Client()
        response = c.get(reverse('Account:login'))
        self.assertEqual(response.status_code, 200)

    def test_login(self):
        c = Client()
        response = c.post(reverse('Account:login'), {'username': 'asdasd', 'password': 'asdasd'})
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Please enter a correct username and password.')
