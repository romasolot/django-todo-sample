from django.contrib.auth.forms import AuthenticationForm, UsernameField, UserCreationForm
from django import forms


class AuthForm(AuthenticationForm):
    username = UsernameField(
        max_length=254,
        widget=forms.TextInput(attrs={'class': 'form-control', 'autofocus': ''}),
    )
    password = forms.CharField(
        label="Password",
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
    )


class RegistrationForm(UserCreationForm):
    username = UsernameField(
        max_length=254,
        widget=forms.TextInput(attrs={'class': 'form-control', 'autofocus': ''}),
    )

    password1 = forms.CharField(
        label="Password",
        strip=False,
        widget=forms.PasswordInput(attrs={'class': 'form-control'})
    )
    password2 = forms.CharField(
        label="Password confirmation",
        widget=forms.PasswordInput(attrs={'class': 'form-control'}),
        strip=False,
        help_text="Enter the same password as before, for verification.",
    )
