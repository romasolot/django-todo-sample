from django.conf.urls import url
from django.contrib.auth import views as auth_views

from todo_auth.forms import AuthForm
from todo_auth.views import RegistrationView

urlpatterns = [
    url(r'^login$',
        auth_views.login,
        {'template_name': 'todo_auth/login.html', 'authentication_form': AuthForm},
        name='login'),
    url(r'^registration', RegistrationView.as_view(), name='registration'),
    url(r'^logout$', auth_views.logout, name='logout')
]
