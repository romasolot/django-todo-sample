from django.contrib.auth import login
from django.shortcuts import redirect
from django.views.generic import FormView

from todo_auth.forms import RegistrationForm


class RegistrationView(FormView):
    form_class = RegistrationForm
    template_name = 'todo_auth/registration.html'

    def get_success_url(self):
        return '/'

    def form_valid(self, form):
        if form.is_valid():
            form.save()
            login(self.request, form.instance)
            return redirect(self.get_success_url())
